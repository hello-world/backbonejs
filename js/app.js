/**
 * app.js
 * Demo of backbonejs
 * Bruce Jia
 * 6/22/2014
 */
$(function(){
	var MyModel = Backbone.Model.extend({
		initialize: function(){
	       log("Initialize myModel. " + JSON.stringify(this.defaults));
	    },
	    defaults: {
	        name: 'Default title',
	        age: 2011,
	    },
	    url: "/todo,restful URL"
	});

	var MyView = Backbone.View.extend({
		el: $("#id"), //页面元素
		model: new MyModel(),
		template: _.template("<div>hi, the name is <%= name %> and age is <%= age %></div>"),
		initialize: function(){
			log("Initialize MyView");
			this.listenTo(this.model, "change:age", this.changeAge);
			this.listenTo(this.model, "change", this.render);
			this.listenTo(this.model, "sync", this.render);// this.model.fetch() 会自动触发render 
			this.render();
			//this.model.fetch();
		},
		render : function() {
		    $("#info").html(this.template(this.model.toJSON()));
		},
	  	events: {
	        'click .btn': 'handleClick'
	    },
		handleClick: function(){
			log("<br/>Get model.name = " + this.model.get("name"));
			var newName = "FIFA";
			log("Set model.name = " + newName);
	      	this.model.set("name", newName);
			log("Get new Name: " + this.model.get("name"));
			log("Changing age...");
			this.model.set("age","2014");
		},
		changeAge: function(){
			log("Fire listener, get user age: " + this.model.get("age"));
		}
	});

	var myView = new MyView();

	//Router
	var AppRouter = Backbone.Router.extend({
        routes: {
            "posts/:id": "getPost"
        },
        getPost: function( id ) {
            // Note the variable in the route definition being passed in here
            alert( "Get post number " + id );   
        },
        defaultRoute: function( actions ){
            alert( actions ); 
        }
    });
    // Instantiate the router
    var app_router = new AppRouter();
	Backbone.history.start();  
});